#!/usr/bin/python3
# vim: set cin fileencoding=utf-8 et ts=4 sw=4 tw=80:

import sys
import os
import time
import datetime as dt
import re
from os import path
import feedparser as fp
from urllib import parse

def topicsearch(top):
    '''
    Given an input topic "top", returns a list of the five latest articles
    uploaded to arXiv matching "top" in its title. The output is a list in
    Markdown format.

    Example
    -------
    :~> python3 topic.py 
    [Search for sterile neutrino mixing using three years of IceCube DeepCore data](http://arxiv.org/abs/1702.05160v1)
    _IceCube Collaboration_

    [Sterile Neutrino Assisted Dominant Seesaw with Lepton Flavor and Number Violations and Leptogenesis](http://arxiv.org/abs/1607.07236v3)
    _M. K. Parida, Bidyut Prava Nayak_

    [Sterile neutrino dark matter from right-handed neutrino oscillations](http://arxiv.org/abs/1702.04526v1)
    _Kenji Kadota, Kunio Kaneta_

    [Anomalies in (Semi)-Leptonic $B$ Decays $B^{\pm} \to τ^{\pm} ν$, $B^{\pm} \to D τ^{\pm} ν$ and $B^{\pm} \to D^* τ^{\pm} ν$, and Possible Resolution with Sterile Neutrino](http://arxiv.org/abs/1702.04335v1)
    _G. Cvetič, Francis Halzen, C. S. Kim, Sechul Oh_

    [Closing in on Resonantly Produced Sterile Neutrino Dark Matter](http://arxiv.org/abs/1701.07874v2)
    _John F. Cherry, Shunsaku Horiuchi_
    '''

    url = 'http://export.arxiv.org/api/query?search_query=ti:"{:s}"&sortBy=lastUpdatedDate&sortOrder=descending&start=0&max_results=5'

    furl    = parse.quote(url.format(top), safe='&/:?=')
#    print(furl)
    d       = fp.parse(furl)
    entries = d.entries

    if len(entries) == 0:
        return('No results found for topic: {:s}'.format(top))

#    print(entries)
#    print('\n\n')
#    return()

    infostr = ''
    for nitem, item in enumerate(entries):
        titstr = item.title.split()
        titstr = ' '.join(titstr)
        auths  = []
        if 'Collaboration' in item.authors[0].name:
            auths.append(item.authors[0].name)
        else:
            nauths = len(item.authors)
            for na, auth in enumerate(item.authors):
                if (nauths > 10):
                    if na == 10:
                        auths.append('et al')
                        break
                if 'Collaboration' in auth.name:
                    break;
                
                auths.append(auth.name)
        
        authors  = ', '.join(auths)
        summ     = item.summary
        arxid    = item.id
        link     = item.link
#        print(item.doi)
        pubcheck = ''
        doi      = ''
        try:
            if item.arxiv_doi:
                pubcheck = u'✅'
                doi = parse.quote(item.arxiv_doi, safe='/:')

        except:
            pass         

        infostr += '[{:s}]({:s}) {:s}\n_{:s}_\n'.format(titstr, link, pubcheck, authors)
        if doi:
            infostr += '[{:s}](https://dx.doi.org/{:s})\n\n'.format(item.arxiv_doi, doi)
        else:
            infostr += '\n'

#    print(infostr)
    return(infostr)

if __name__ == '__main__':
    print(topicsearch(' '.join(sys.argv[1:]) if len(sys.argv) >= 2 else "superluminal neutrino"))
