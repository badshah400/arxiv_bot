#!/usr/bin/python3
# vim: set cin fileencoding=utf-8 et ts=4 sw=4 tw=80:

import telepot
from todaysfeed import *
from arxid import *
from topic import *
from author import *

TOKEN = '347059652:AAEw3a8NBj_8dohR93lEYHEasG3uTXuy9BY'

bot = telepot.Bot(TOKEN)

def commhelp():
    mess  = 'The following commands are currently defined:\n'
    mess += ('```\n'
             '/arxivid - For one input arxiv identifier, display corresponding info.\n\n'
             '/today - For one valid arxiv subject class, display today\'s feed for that subject.\n\n'
             '/topicsearch - Given a topic, display five latest preprints with title matches to given topic.\n\n'
             '/authsearch - Given an author, display five latest preprints by said author.\n\n'
             '/help - Display this message.```'
            )
    return(mess)

def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)

    if content_type == 'text':
        msgs = msg['text'].split()
        if ((msgs[0] == '/arxivid') or (msgs[0] == '/arxivid@Arxiv_ID_bot')):
            if len(msgs) != 2:
                bot.sendMessage(chat_id, "One arXiv ID input required.")
            else:
                bot.sendMessage(chat_id, arxivcontent(msgs[1]),
                                parse_mode='Markdown')

        elif ((msgs[0] == '/today') or (msgs[0] == '/today@Arxiv_ID_bot')):
            bot.sendMessage(chat_id, arXiv_todaysfeed(msgs[1] if len(msgs) == 2 else 'hep-ph'),
                            parse_mode='Markdown')

        elif ((msgs[0] == '/topicsearch') or (msgs[0] == '/topicsearch@Arxiv_ID_bot')):
            if len(msgs) < 2:
                bot.sendMessage(chat_id, "At least a one-word topic required.")
            top = ' '.join(msgs[1:])
            print(top)
            bot.sendMessage(chat_id, topicsearch(top),
                            parse_mode='Markdown')

        elif ((msgs[0] == '/authsearch') or (msgs[0] == '/authsearch@Arxiv_ID_bot')):
            if len(msgs) < 2:
                bot.sendMessage(chat_id, "At least a one-word author name required.")
            top = ' '.join(msgs[1:])
            print(top)
            bot.sendMessage(chat_id, authsearch(top),
                            parse_mode='Markdown')

        elif ((msgs[0] == '/help') or (msgs[0] == '/help@Arxiv_ID_bot')):
                bot.sendMessage(chat_id, commhelp(), parse_mode='Markdown')

        else:
            bot.sendMessage(chat_id, 'No valid command issued.')

bot.message_loop(handle)
print ('Listening ...')

# Keep the program running.
while 1:
    time.sleep(10)

