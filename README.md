#Commands for arXiv parsing bot on telegram#

This is a collection of commands used by the arXiv parser bot for [telegram](https://telegram.org/). The latest version can always be obtained from the [Bitbucket repository](https://bitbucket.org/badshah400/arxiv_bot). The code is being distributed in accordance with the GPL-3.0 license. See [COPYING](COPYING) file for details.

The conversion of the appropriate python functions to the corresponding
telegram bot commands is done by [mainbot.py](mainbot.py).

##Summary of commands##
The following commands are currently implemented. Here is a quick summary. Detailed explanations and examples follow (which show their usage from a python term).

* `/help` - Displays a summary of available commands
* `/authsearch` - Given an input author _Rookie Author_, returns a list of the five latest articles uploaded to arXiv where _Rookie Author_ is at least one of the authors.
* `/arxivid` - Given an input arXiv identifier (e.g. `1607.00193`), print the corresponding article's title, arXiv URL, authors and summary. For published work, also print the corresponding DOI URL.
* `/today` - Fetches today's feed from arXiv for the subject class `subjcl` and displays the first five in a Markdown formatted list. Also indicate when an item is really a replacement by placing an up icon "⬆"
* `/topic` - Given an input topic "top", returns a list of the five latest articles uploaded to arXiv matching "top" in its title.


###`/arxivid`###

File: [arxid.py](arxid.py)

Given an input arXiv identifier (e.g. 1607.00193), print the corresponding
article's title, arXiv URL, authors and summary. For published work, also
print the corresponding DOI URL. The output is formatted in MarkDown format.

**Example**

    :~> python3 arxid.py hep-ph/9203201
    [Comments on the Electroweak Phase Transition](http://arxiv.org/abs/hep-ph/9203201v1)
    _M. Dine, R. G. Leigh, P. Huet, A. D. Linde and D. A. Linde_
    ```
    We report on an investigation of various problems related to the theory of the electroweak phase transition. This includes a determination of the nature of the phase transition, a discussion of the possible role of higher order radiative corrections and the theory of the formation and evolution of the bubbles of the new phase. We find in particular that no dangerous linear terms appear in the effective potential. However, the strength of the first order phase transition is 2/3 times less than what follows from the one-loop approximation. This rules out baryogenesis in the minimal version of the electroweak theory.```
    [10.1016/0370-2693(92)90026-Z](https://dx.doi.org/10.1016/0370-2693(92)90026-Z)



###`/authsearch`###

File: [author.py](author.py)

Given an input author _Rookie Author_, returns a list of the five latest
articles uploaded to arXiv where _Rookie Author_ is at least one of the
authors. The output is a list in Markdown format. "✅" indicates a published
work.

**Example**

    :~> python3 author.py Atri Bhattacharya
    [Boosted Dark Matter and its implications for the features in IceCube HESE data](http://arxiv.org/abs/1612.02834v2) 
    _Atri Bhattacharya, Raj Gandhi, Aritra Gupta, Satyanarayan Mukhopadhyay_

    [Prompt atmospheric neutrino flux from the various QCD models](http://arxiv.org/abs/1611.05540v2) 
    _Yu Seon Jeong, Atri Bhattacharya, Rikard Enberg, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Prompt atmospheric neutrino flux](http://arxiv.org/abs/1611.05120v1) 
    _Yu Seon Jeong, Atri Bhattacharya, Rikard Enberg, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Prompt atmospheric neutrino fluxes: perturbative QCD models and nuclear effects](http://arxiv.org/abs/1607.00193v2) ✅
    _Atri Bhattacharya, Rikard Enberg, Yu Seon Jeong, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Cold atoms in U(3) gauge potentials](http://arxiv.org/abs/1603.08526v2) ✅
    _Ipsita Mandal, Atri Bhattacharya_


###`/today`###

File: [todaysfeed.py](todaysfeed.py)

Fetches today's feed from arXiv for the subject class subjcl and displays
the first five in a Markdown formatted list. Also indicate when an item is
really a replacement by placing an up icon "⬆".

Since the total number of items in a day's digest can be rather large, do
not print them all at once. Instead provide a link for follow-up.

**Example**

    :~> python3 todaysfeed.py hep-ph
    [Weak Boson Fusion at 100 TeV](http://arxiv.org/abs/1702.05098)
    1702.05098 
    _Dorival Goncalves, Tilman Plehn, Jennifer M. Thompson_
    
    [On new physics searches with multidimensional differential shapes](http://arxiv.org/abs/1702.05106)
    1702.05106 
    _Felipe Ferreira, Sylvain Fichet, Veronica Sanz_
    
    [Re-examining valence quark spin distributions](http://arxiv.org/abs/1702.05152)
    1702.05152 
    _A. I. Signal_
    
    [Search for sterile neutrino mixing using three years of IceCube DeepCore data](http://arxiv.org/abs/1702.05160)
    1702.05160 
    _IceCube Collaboration_
    
    [Charmless two-body anti-triplet $b$-baryon decays](http://arxiv.org/abs/1702.05263)
    1702.05263 
    _Y.K. Hsiao, Yu Yao, C.Q. Geng_
    
    28 more at https://arxiv.org/list/hep-ph/new
    


###`/topicsearch`###

File: [topic.py](topic.py)

Given an input topic "top", returns a list of the five latest articles
uploaded to arXiv matching "top" in its title. The output is a list in
Markdown format.

**Example**

    :~> python3 topic.py sterile neutrino
    [Search for sterile neutrino mixing using three years of IceCube DeepCore data](http://arxiv.org/abs/1702.05160v1)
    _IceCube Collaboration_

    [Sterile Neutrino Assisted Dominant Seesaw with Lepton Flavor and Number Violations and Leptogenesis](http://arxiv.org/abs/1607.07236v3)
    _M. K. Parida, Bidyut Prava Nayak_

    [Sterile neutrino dark matter from right-handed neutrino oscillations](http://arxiv.org/abs/1702.04526v1)
    _Kenji Kadota, Kunio Kaneta_

    [Anomalies in (Semi)-Leptonic $B$ Decays $B^{\pm} \to τ^{\pm} ν$, $B^{\pm} \to D τ^{\pm} ν$ and $B^{\pm} \to D^* τ^{\pm} ν$, and Possible Resolution with Sterile Neutrino](http://arxiv.org/abs/1702.04335v1)
    _G. Cvetič, Francis Halzen, C. S. Kim, Sechul Oh_

    [Closing in on Resonantly Produced Sterile Neutrino Dark Matter](http://arxiv.org/abs/1701.07874v2)
    _John F. Cherry, Shunsaku Horiuchi_
