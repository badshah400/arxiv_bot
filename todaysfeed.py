#!/usr/bin/python3
# vim: set fileencoding=utf-8 cin et ts=4 sw=4 tw=80:

import sys
import os
import time
from os import path
from lxml import etree
import feedparser as fp

ARXTAGS = {'astro-ph', 'cond-mat', 'gr-qc',
           'hep-ex', 'hep-lat', 'hep-ph', 'hep-th',
           'math-ph', 'nlin', 'nucl-ex', 'nucl-th',
           'physics', 'quant-ph', 'math', 'CoRR',
           'q-bio', 'q-fin', 'stat'}


def arXiv_todaysfeed(subjcl):
    '''
    Fetches today's feed from arXiv for the subject class subjcl and displays
    the first five in a Markdown formatted list. Also indicate when an item is
    really a replacement by placing an up icon "⬆".

    Since the total number of items in a day's digest can be rather large, do
    not print them all at once. Instead provide a link for follow-up.
    '''

    if subjcl not in ARXTAGS:
        print('Invalid subject class received.')
        return('Invalid subject class received.')
        
    url = 'http://export.arxiv.org/rss/{:s}?version=2.0'.format(subjcl)
    d   = fp.parse(url)
    infostr = ''

    nitems = len(d.entries)
    for cnt, entry in enumerate(d.entries):
        if (cnt == 5):
            infostr += '{:d} more at https://arxiv.org/list/{:s}/new'.format(nitems-5,subjcl)
            break

        tparts = entry.title.split('.')
        titstr = tparts[0]

        update = u'⬆' if 'UPDATED' in tparts[-1] else ''
        arxid  = entry.id.split(':')[-1]

        summ = entry.summary
        ht = etree.HTML(summ)
        elems = ht.xpath("//text()")

        auths = []

        if 'Collaboration' in elems[1]:
            authstr = elems[1]
        
        else:
            for i, el in enumerate(elems):
                if not i:
                    continue
                 
                if el == '\n\n':
                    abstr = elems[i+1]
                    break
                 
                elif el != ', ':
                    auths.append(el)

            authstr = ', '.join(auths)
        url = entry.link
#        print(url)
        infostr += '[{:s}]({:s})\n{:s} {:s}\n_{:s}_\n\n'.format(titstr, url, arxid, update, authstr)

    return(infostr)

if __name__ == '__main__':
    print(arXiv_todaysfeed(sys.argv[1] if len(sys.argv) > 1 else 'hep-ph'))
