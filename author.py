#!/usr/bin/python3
# vim: set fileencoding=utf-8 cin et ts=4 sw=4 tw=80:

import sys
import os
import time
import datetime as dt
import re
from os import path
import feedparser as fp
from urllib import parse

def authsearch(author_name):
    '''
    Given an input author "Rookie Author", returns a list of the five latest
    articles uploaded to arXiv where "Rookie Author" is at least one of the
    authors. The output is a list in Markdown format. "✅" indicates a published
    work.

    Example
    -------
    :~> python3 author.py Atri Bhattacharya
    [Boosted Dark Matter and its implications for the features in IceCube HESE data](http://arxiv.org/abs/1612.02834v2) 
    _Atri Bhattacharya, Raj Gandhi, Aritra Gupta, Satyanarayan Mukhopadhyay_

    [Prompt atmospheric neutrino flux from the various QCD models](http://arxiv.org/abs/1611.05540v2) 
    _Yu Seon Jeong, Atri Bhattacharya, Rikard Enberg, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Prompt atmospheric neutrino flux](http://arxiv.org/abs/1611.05120v1) 
    _Yu Seon Jeong, Atri Bhattacharya, Rikard Enberg, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Prompt atmospheric neutrino fluxes: perturbative QCD models and nuclear effects](http://arxiv.org/abs/1607.00193v2) ✅
    _Atri Bhattacharya, Rikard Enberg, Yu Seon Jeong, C. S. Kim, Mary Hall Reno, Ina Sarcevic, Anna Stasto_

    [Cold atoms in U(3) gauge potentials](http://arxiv.org/abs/1603.08526v2) ✅
    _Ipsita Mandal, Atri Bhattacharya_
    '''

    url = 'http://export.arxiv.org/api/query?search_query=au:"{:s}"&sortBy=lastUpdatedDate&sortOrder=descending&start=0&max_results=5'

    furl    = parse.quote(url.format(author_name), safe='&/:?=')
#    print(furl)
    d       = fp.parse(furl)
    entries = d.entries

    if len(entries) == 0:
        return('No results found for topic: {:s}'.format(author_name))

#    print(entries)
#    print('\n\n')
#    return()

    infostr = ''
    for nitem, item in enumerate(entries):
        titstr = item.title.split()
        titstr = ' '.join(titstr)
        auths  = []
        if 'Collaboration' in item.authors[0].name:
            auths.append(item.authors[0].name)
        else:
            nauths = len(item.authors)
            for na, auth in enumerate(item.authors):
                if (nauths > 10):
                    if na == 10:
                        auths.append('et al')
                        break
                if 'Collaboration' in auth.name:
                    break;
                
                auths.append(auth.name)
        
        authors  = ', '.join(auths)
        summ     = item.summary
        arxid    = item.id
        link     = item.link
        pubcheck = ''
        doi      = ''
        try:
            if item.arxiv_doi:
                pubcheck = u'✅'
                doi = parse.quote(item.arxiv_doi, safe='/:')

        except:
            pass         

        infostr += '[{:s}]({:s}) {:s}\n_{:s}_\n'.format(titstr, link, pubcheck, authors)
        if doi:
            infostr += '[{:s}](https://dx.doi.org/{:s})\n\n'.format(item.arxiv_doi, doi)
        else:
            infostr += '\n'

#    print(infostr)
    return(infostr)

if __name__ == '__main__':
    print(authsearch(' '.join(sys.argv[1:]) if len(sys.argv) >= 2 else "Steven Weinberg"))
