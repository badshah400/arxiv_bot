#!/usr/bin/python3
# vim: set cin fileencoding=utf-8 et ts=4 sw=4 tw=80:

import sys
import os
import time
import datetime as dt
import re
from os import path
import feedparser as fp
from urllib import parse

ARXTAGS = {'astro-ph', 'cond-mat', 'gr-qc',
           'hep-ex', 'hep-lat', 'hep-ph', 'hep-th',
           'math-ph', 'nlin', 'nucl-ex', 'nucl-th',
           'physics', 'quant-ph', 'math', 'CoRR',
           'q-bio', 'q-fin', 'stat'}


def validate(aid):
    '''
    Validate input arXiv identifier aid.
    '''

    res = False

    # OLD-FORM
    oldidparts = aid.split('/')
    if len(oldidparts) == 2:
        if oldidparts[0] in ARXTAGS:
#            print(oldidparts[1])
            idpatt = re.compile('910[7-9][0-9]{3}|911[0-2][0-9]{3}')
            if idpatt.match(oldidparts[1]):
#                print(1)
                return(True)
            idpatt = re.compile('9[2-9]0[1-9][0-9]{3}|9[2-9]1[0-2][0-9]{3}')
            if idpatt.match(oldidparts[1]):
#                print(2)
                return(True)
            idpatt = re.compile('0[0-6]0[1-9][0-9]{3}|0[0-6]1[0-2][0-9]{3}')
            if idpatt.match(oldidparts[1]):
#                print(3)
                return(True)
            idpatt = re.compile('070[1-3][0-9]{3}')
            if idpatt.match(oldidparts[1]):
#                print(4)
                return(True)

    aidpatt = re.compile('0[7-9]0[1-9]\.[0-9]{4}|1[0-4]0[1-9]\.[0-9]{4}')
    match   = aidpatt.match(aid)
    if match:
        return(True)

    aidpatt = re.compile('0[7-9]1[0-2]\.[0-9]{4}|1[0-4]1[0-2]\.[0-9]{4}')
    match   = aidpatt.match(aid)
    if match:
        return(True)

    aidpatt = re.compile('1[5-9]0[1-9]\.[0-9]{5}|1[5-9]1[0-2]\.[0-9]{5}')
    match   = aidpatt.match(aid)
    if match:
        tdyymm = dt.date.today()
        yymm = aid.split('.')[0]
        dtdiff = int(yymm) - int(tdyymm.strftime("%y%m"))
        return(True if dtdiff <= 0 else False)

    return(res)

def arxivcontent(aid):
    '''
    Given an input arXiv identifier (e.g. 1607.00193), print the corresponding
    article's title, arXiv URL, authors and summary. For published work, also
    print the corresponding DOI URL. The output is formatted in MarkDown format.

    Example
    -------
    :~> python3 arxid.py hep-ph/9203201
[Comments on the Electroweak Phase Transition](http://arxiv.org/abs/hep-ph/9203201v1)
_M. Dine, R. G. Leigh, P. Huet, A. D. Linde and D. A. Linde_
```
We report on an investigation of various problems related to the theory of the electroweak phase transition. This includes a determination of the nature of the phase transition, a discussion of the possible role of higher order radiative corrections and the theory of the formation and evolution of the bubbles of the new phase. We find in particular that no dangerous linear terms appear in the effective potential. However, the strength of the first order phase transition is 2/3 times less than what follows from the one-loop approximation. This rules out baryogenesis in the minimal version of the electroweak theory.```
[10.1016/0370-2693(92)90026-Z](https://dx.doi.org/10.1016/0370-2693(92)90026-Z)

    '''

    #aid  = sys.argv[1] if len(sys.argv) == 2 else '1607.00193'

    # Validate aid for arxiv ID [post 2007 only]

    if not validate(aid):
        mess = 'Invalid arXiv identifier passed.'
        print(mess)
        return(mess)

    url  = 'http://export.arxiv.org/api/query?id_list={:s}'.format(aid)
    d    = fp.parse(url)

#    print(d.entries)

    if len(d.entries) == 0 or ('title' not in d.entries[0]):
        mess = 'No info for arXiv identifier {:s}'.format(aid)
        print(mess)
        return(mess)

    item = d.entries[0]
    dispurl = item.link
    title = item.title.split()
    title = ' '.join(title)
    bann  = ''
    for i in range(0, len(title)):
        bann += '-'
#    print(bann)
#    print(title)
#    print(bann)
    summ  = item.summary.split()
    summ  = ' '.join(summ) 
#    print(summ)

    authstr = ''
    nosauth = len(item.authors)
    for nos, auth in enumerate(item.authors):
        if nos == (len(item.authors)-2):
            authstr += '{:s} and '.format(auth.name)
        elif nos == (len(item.authors)-1):
            authstr += '{:s}'.format(auth.name)
        else:
            authstr += '{:s}, '.format(auth.name)

    fullstr = '[{:s}]({:s})\n_{:s}_\n```\n{:s}```'.format(title,dispurl,authstr,summ)

    if 'arxiv_doi' in item:
        doi = parse.quote(item.arxiv_doi, safe='/:')
        fullstr += '\n' + r'[{0:s}](https://dx.doi.org/{0:s})'.format(doi)
#    return('{:s} ```{:s}```'.format(fullstr,summ)
    return(fullstr)

if __name__ == '__main__':
    print(arxivcontent(sys.argv[1] if len(sys.argv) >= 2 else '1501.00001'))

